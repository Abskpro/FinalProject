const validateRegisterInput = require('../validation/register.validate.js');
const validateLoginInput = require('../validation/login.validate.js');
const key = require('../config/key.js');
const {v4: uuidv4} = require('uuid');
const nodemailer = require('nodemailer');
const User = require('../models/user.model');
const Token = require('../models/token.model.js');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

//mail service
var smtpTrasport = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: 'jonathan148369@gmail.com', //generated by Mailtrap
    pass: 'p@ssworD',
  },
});

//register new user
const registerUser = (req, res, next) => {
  const {errors, isValid} = validateRegisterInput(req.body);

  //check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({email: req.body.email}).then(user => {
    if (user) {
      return res.status(400).json({email: 'email already exists'});
    } else {
      const newUser = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
      });

      //hash password before saving in databse
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          newUser
            .save()
            .then(user => {
              const token = uuidv4();
              const newToken = new Token({
                _userId: user._id,
                token: token,
              });
              newToken.save(function (err) {
                if (err) {
                  return res.status(500).send({msg: err.message});
                }

                let link = `http://${req.get(
                  'host',
                )}/api/user/verify?id=${token}`;
                mailOptions = {
                  from: 'Admin <abc@gmail.com>',
                  to: `${req.body.email}`,
                  subject: 'Please confirm you Email account',
                  text: `Hello ${req.body.name}`, // plaintext body
                  html:
                    'Hello,<br> Please Click on the link to verify you email.<br><a href=' +
                    link +
                    '>Click here to verify</a>',
                };

                smtpTrasport.sendMail(mailOptions, function (err, response) {
                  if (error) {
                    console.log(error);
                    res.end('error');
                  } else {
                    console.log('message sent' + response.message);
                    res.end('sent');
                  }
                });
              });
            })
            .catch(err => console.log(err));
        });
      });
    }
  });
};

const verifyUser = (req, res) => {
  console.log(req.query.id, req.protocol, req.get('host'));
  Token.findOne({token: req.query.id}).then(token => {
    if (!token) {
      return res.status(400).json({token: 'token doest exist'});
    }
    console.log(token._id, token._userId);
    User.findOneAndUpdate({_id: token._userId}, {isVerified: true}).then(
      res.json({message: 'you have been verified'}),
    );
  });
};

const loginUser = (req, res) => {
  //form validation
  const {errors, isValid} = validateLoginInput(req.body);

  //check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;

  User.findOne({email}).then(user => {
    //check if user exists
    if (!user) {
      return res.status(404).json({emailnotfound: 'Email not found'});
    }

    if (!user.isVerified) {
      return res
        .status(401)
        .json({message: 'User is not verified please verify you account'});
    }

    bcrypt.compare(password, user.password).then(isMatch => {
      if (isMatch) {
        const payload = {
          id: user.id,
          name: user.name,
          profile: user.profile,
        };

        jwt.sign(
          payload,
          key.secretKey,
          {
            expiresIn: 3600,
          },
          (err, token) => {
            res.json({
              success: true,
              token: 'Bearer' + token,
            });
          },
        );
      } else {
        return res.status(400).json({passwordincorrect: 'Passsword incorrect'});
      }
    });
  });
};

module.exports = {registerUser, loginUser, verifyUser};
